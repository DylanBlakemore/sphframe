/*
 * particle.cpp
 *
 *  Created on: 22 Mar 2017
 *      Author: dylan
 */

#include "../headers/particle.h"

Particle::Particle(std::vector<float>& initial_position, int ID)
{
	dim = initial_position.size();
	for(int i = 0; i < dim; i++) {
		x.push_back(initial_position[i]);
		vh.push_back(0);
		a.push_back(0);
		v.push_back(0);
	} //end for
	rho = 0;
	m = 0;
	is_initialized = 0;
	id = ID;
	h = 1;
}

void Particle::updatePosition(float dt)
{
	/**
	 * The first step in the integration is slightly different,
	 * since the "previous" half-step velocity must be initialized.
	 */
	if (!is_initialized) {
		/* Initialize half-step velocity */
		for(int i = 0; i < dim; i++)
			vh[i] = v[i] + a[i]*dt/2;
		/* Initialize velocity */
		for(int i = 0; i < dim; i++)
			v[i] += a[i]*dt;
		/* Initialize position */
		for(int i = 0; i < dim; i++)
			x[i] += vh[i]*dt;

		is_initialized = 1;
	}
	else {
		/* Update half-step velocity */
		for(int i = 0; i < dim; i++)
			vh[i] += a[i]*dt;
		/* Update velocity */
		for(int i = 0; i < dim; i++)
			v[i] = vh[i] + a[i]*dt/2;
		/* Update position */
		for(int i = 0; i < dim; i++)
			x[i] += vh[i]*dt;
	}
}


