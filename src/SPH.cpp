//============================================================================
// Name        : SPH.cpp
// Author      : Dylan Blakemore
// Version     : 0.1.0
// Copyright   : None
// Description : A framework for solving Smooth Particle Hydrodynamics
//				 problems. The method is a simplified one to facilitate
//				 easy understanding and editing rather than to provide
//				 the most efficient solution.
//============================================================================

#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "../headers/particle.h"
#include "../headers/cubic_kernel.h"
#include "../headers/solver.h"

int main()
{
	int n_steps = 400;
	int n_particles = 1000;
	float dt = 0.04;
	int save_increment = 5;

	CubicSplineKernel2D cubic = CubicSplineKernel2D();
	std::vector<Kernel*> kernels;
	kernels.push_back(&cubic);

	std::cout << "Complete" << std::endl;

	return 0;
}
