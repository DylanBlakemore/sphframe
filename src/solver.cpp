/*
 * solver.cpp
 *
 *  Created on: 22 Mar 2017
 *      Author: dylan
 */

#include "../headers/solver.h"

SPHSolver::SPHSolver(std::vector<Particle>& particles_in,
		  	  	  	 std::vector<Kernel*>& kernels_in,
					 int save_increment)
{
	particles = particles_in;
	kernels = kernels_in;
	n_particles = particles.size();
	dim = particles[0].dim;
	save_inc = save_increment;
	iter = 0;
	t = 0;
}

void SPHSolver::update(float dt)
{
	calculateDensities();
	calculateAccelerations();
	iter++;
	for (int i = 0; i < n_particles; i++) {
		if(remainder(iter,save_inc) == 0 || iter == 1) {
			std::vector<float> history_entry;
			history_entry.push_back((float)particles[i].id);
			for(int d = 0; d < particles[i].dim; d++)
				history_entry.push_back(particles[i].x[d]);
			history_entry.push_back(particles[i].rho);
			history_entry.push_back(t);
			history.push_back(history_entry);
		}
		particles[i].updatePosition(dt);
	}
	t += dt;
}

void SPHSolver::calculateDensities()
{
	/** Change this to the kernel used in density calculations */
	Kernel* rho_kernel = kernels[0];

	for (int i = 0; i < n_particles; i++)
		particles[i].rho = particles[i].m * rho_kernel->getWeight(particles[i],particles[i]);

	for (int i = 0; i < n_particles; i++)
	{
		for (int j = i+1; j < n_particles; j++)
		{
			float rho_ij = particles[i].m * rho_kernel->getWeight(particles[i], particles[j]);

			particles[i].rho += rho_ij;
			particles[j].rho += rho_ij;
		}
	}
}

int SPHSolver::saveHistory(std::string fname)
{
	/** Add the final entry to the history. It may not have been added
	 * due to the update step only adding every save_inc'th position.
	 */
	for (int i = 0; i < n_particles; i++) {
		std::vector<float> history_entry;
		history_entry.push_back((float)particles[i].id);
		for(int d = 0; d < particles[i].dim; d++)
			history_entry.push_back(particles[i].x[d]);
		history_entry.push_back(particles[i].rho);
		history_entry.push_back(t);
		history.push_back(history_entry);
	}
	int success = 0;
	CSVFile outfile(fname, ',');
	outfile.open('w');
	outfile.write(&history);
	outfile.close();
	return success;
}
