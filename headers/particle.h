/*
 * particle.h
 *
 *  Created on: 22 Mar 2017
 *      Author: dylan
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <vector>

class Particle {
public:
	Particle(std::vector<float>& initial_position, int ID);

	void updatePosition(float dt);

	std::vector<float> x;
	std::vector<float> v;
	std::vector<float> vh;
	std::vector<float> a;
	float rho;
	float m;
	float h;
	float id;
	float dim;

private:
	bool is_initialized;
};



#endif /* PARTICLE_H_ */
