/*
 * solver.h
 *
 *  Created on: 22 Mar 2017
 *      Author: dylan
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <vector>
#include <string>

#include "kernel.h"
#include "particle.h"
#include "csv.h"

class SPHSolver
{
public:
	SPHSolver(std::vector<Particle>& particles_in,
			  std::vector<Kernel*>& kernels_in,
			  int save_increment=1);

	void update(float dt);
	int saveHistory(std::string fname);
	virtual ~SPHSolver(){}

protected:
	void calculateDensities();
	virtual void calculateAccelerations(){}
	std::vector<float> calcBodyForce(Particle& p1, Particle& p2);
	std::vector<float> calcViscousForce(Particle& p1, Particle& p2);
	std::vector<float> calcPressureForce(Particle& p1, Particle& p2);
	std::vector<Particle> particles;
	std::vector<Kernel*> kernels;
	std::vector<std::vector<float> > history;
	int n_particles;
	int dim;
	int save_inc;
	int iter;
	float t;
};



#endif /* SOLVER_H_ */
