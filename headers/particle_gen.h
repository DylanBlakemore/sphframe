/*
 * particle_gen.h
 *
 *  Created on: 23 Mar 2017
 *      Author: dylan
 */

#ifndef PARTICLE_GEN_H_
#define PARTICLE_GEN_H_

#include <vector>
#include "particle.h"

class ParticleGenerator
{
	ParticleGenerator();
	virtual std::vector<Particle> create();
	virtual ~ParticleGenerator();
};





#endif /* PARTICLE_GEN_H_ */
