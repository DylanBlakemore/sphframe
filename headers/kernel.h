/*
 * kernel.h
 *
 *  Created on: 22 Mar 2017
 *      Author: dylan
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include <vector>
#include <math.h>
#include "particle.h"

class Kernel {
public:
	Kernel() {
	}

	virtual float getWeight(Particle& p1, Particle& p2)
	{
		return 0;
	}

	virtual float getGradientCoefficient(Particle& p1, Particle& p2)
	{
		return 0;
	}

	virtual float getLaplacian(Particle& p1, Particle& p2)
	{
		return 0;
	}

	virtual ~Kernel(){}

protected:
	float getDistance(Particle& p1, Particle& p2)
	{
		float distance = 0;
		for(int i = 0; i < p1.dim; i++)
			distance += pow(p1.x[i] - p2.x[i],2);
		return sqrt(distance);
	}
};



#endif /* KERNEL_H_ */
