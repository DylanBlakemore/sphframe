/*
 * cubic_kernel.h
 *
 *  Created on: Mar 7, 2017
 *      Author: user
 */

#ifndef CUBIC_KERNEL_H_
#define CUBIC_KERNEL_H_

#include <vector>
#include <math.h>
#include "kernel.h"

using namespace std;

class CubicSplineKernel2D : public Kernel {
public:
	CubicSplineKernel2D() :Kernel() {
	}
	/* Returns the weighting value */
	virtual float getWeight(Particle& p1, Particle& p2) {
		float h = p1.h;
		float r = getDistance(p1, p2);
		float weight = 0;
		float q = r/h;
		float Ch = 5.0 / (14.0 * M_PI * pow(h,2));

		if (q >= 2)
			weight = 0;
		else if (q >= 1)
			weight = pow(2 - q,3);
		else if (q >= 0 && q < 1)
			weight = pow(2 - q,3) - 4*pow(1 - q,3);
		return weight * Ch;
	}

	/* Returns the gradient of the weighting function as a vector */
	virtual float getGradientCoefficient(Particle& p1, Particle& p2) {
		float coeff = 0;
		float h = p1.h;
		float r = getDistance(p1, p2);
		float Ch = 5.0 / (14.0 * M_PI * pow(h,2));

		if (r > 0)
		{
			float q = r/h;
			float u = 2 - q;
			float v = pow(r,2);
			float w = 1 - q;

			float df1 = -3*pow(u,2)*pow(h,-1)*pow(v,-1/2);
			float df2 = -3*pow(w,2)*pow(h,-1)*pow(v,-1/2);

			if (q >= 2)
				coeff = 0;
			else if (q >= 1)
				coeff = Ch * df1;
			else if (q >= 0 && q < 1)
				coeff = Ch * (df1 - 4*df2);
		}
		return coeff;
	}

	/* Returns the laplacian of the weighting function */
	virtual float getLaplacian(Particle& p1, Particle& p2) {
		float laplacian = 0;
		float h = p1.h;
		float Ch = 5.0 / (14.0 * M_PI * pow(h,2));

		return laplacian * Ch;
	}
};



#endif /* CUBIC_KERNEL_H_ */
